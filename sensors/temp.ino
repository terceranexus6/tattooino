float temp;
int tempPin = 0;
int buzzerPin = 9;   

//PONER AQUI LA TEMPERATURA CALIBRADA
int maquina_tatu = 10;

void setup() {
   Serial.begin(9600);
   pinMode(buxxerPin, OUTPUT);
   beep(50);
}

void beep(unsigned char delayms){

  analogWrite(buzzerPin, 20);                                      
  delay(delayms);                                                             
  analogWrite(buzzerPin ,0);             
  delay(delayms);
}

void loop() {
   temp = analogRead(tempPin);
   // read analog volt from sensor and save to variable temp
   temp = temp * 0.48828125; //convert to c
   // convert the analog volt to its temperature equivalent
   Serial.print("TEMPERATURE = ");
   Serial.print(temp); // display temperature value
   Serial.print("*C");
   Serial.println();

   if(temp > maquina_tatu){
   	beep(50);
	delay(500);
	beep(50);
   }

   delay(1000); // update sensor reading each one second
}
